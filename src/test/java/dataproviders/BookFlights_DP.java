package dataproviders;

import org.testng.annotations.DataProvider;
import java.io.IOException;

import static utils.ExcelUtils.getExcelDataTableArray;

public class BookFlights_DP {
    @DataProvider
    public static Object[][] bookAFlightAndCar_DP() throws IOException {
        return getExcelDataTableArray(System.getProperty("user.dir")+
                "/src/test/resources/bookAFlightAndCar.xlsx");
    }
}
