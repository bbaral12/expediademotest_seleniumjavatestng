package pageobjects;

import baseobjects.BasePage;
import utils.customlocators.ByGlobalDataAttribute;
import utils.customlocators.ByTagText;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CarSearchResults extends BasePage {

    private final By chooseYourCarLabelLocator = new ByTagText(
            "span", "Choose your car");

    public CarSearchResults(WebDriver driver){
        super(driver);
    }

    public void isLoaded() {
        wait.until(ExpectedConditions.urlContains("www.expedia.com/carsearch/"));
        wait.until(ExpectedConditions.visibilityOfElementLocated(chooseYourCarLabelLocator));

    }

    public String getTitle() {
        return driver.getTitle();
    }

    public void selectCarFilter(String carType) {
        wait.until(ExpectedConditions.elementToBeClickable(
                new ByGlobalDataAttribute("filter-name", carType)));
        driver.findElement(new ByGlobalDataAttribute("filter-name", carType)).click();
        wait.until(ExpectedConditions.elementToBeClickable(new ByTagText(
                "span", "Economy")));
    }

    public void selectCar(){
        //Select car
        WebElement carResult1 = driver.findElement(By.id("car-offer-cards-listing-0"));
        WebElement selectButton = carResult1.findElement(new ByTagText(
                "span", "Select"))
                .findElement(By.xpath("./.."));
        wait.until(ExpectedConditions.elementToBeClickable(selectButton));
        selectButton.click();
    }
}
