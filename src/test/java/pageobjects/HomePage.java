package pageobjects;

import baseobjects.BasePage;
import utils.customlocators.ByGlobalDataAttribute;
import utils.customlocators.ByTagText;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class HomePage extends BasePage {

    private final By flightsLocator = new ByTagText("span", "Flights");
    private final By roundTripLocator = new ByTagText("span", "Roundtrip");
    private final By searchButtonLocator = new ByTagText("button", "Search");
    private final By departDateLocator = By.id("d1-btn");
    private final By returnDateLocator = By.id("d2-btn");
    private final By applyDateLocator = new ByGlobalDataAttribute(
            "stid", "apply-date-picker");

    public HomePage(WebDriver driver){
        super(driver);
    }

    public String getTitle(){
        return driver.getTitle();
    }

    public void searchRoundTripFlights(String flightOrigin, String flightDestination, String departDate, String returnDate){
        driver.findElement(flightsLocator).click();
        driver.findElement(roundTripLocator).click();
        setLocation("origin", flightOrigin);
        setLocation( "destination", flightDestination);
        selectDates(departDate, returnDate);
        driver.findElement(searchButtonLocator).click();
    }

    private void selectDates(String departDate, String returnDate) {
        driver.findElement(departDateLocator).click();

        By departDateButtonLocator = By.cssSelector("button[aria-label='"+departDate+", 2020'");
        By returnDateButtonLocator = By.cssSelector("button[aria-label='"+returnDate+", 2020'");

        wait.until(ExpectedConditions.elementToBeClickable(departDateButtonLocator));
        driver.findElement(departDateButtonLocator).click();
        wait.until(ExpectedConditions.elementToBeClickable(returnDateButtonLocator));
        driver.findElement(returnDateButtonLocator).click();
        driver.findElement(applyDateLocator).click();

        Assert.assertTrue(driver.findElement(departDateLocator).getText().equals(departDate));
        Assert.assertTrue(driver.findElement(returnDateLocator).getText().equals(returnDate));
    }

    private void setLocation(String leg, String airportCode) {
        By fieldLocator = new ByGlobalDataAttribute(
                "stid","location-field-leg1-" + leg + "-menu-trigger");

        driver.findElement(fieldLocator).click();
        driver.findElement(By.id("location-field-leg1-" + leg)).sendKeys(airportCode);
        driver.findElement(new ByGlobalDataAttribute(
                "stid","location-field-leg1-" + leg + "-result-item-button")).click();

        Assert.assertTrue((driver.findElement(fieldLocator).getText()).contains(airportCode));
    }
}
