package pageobjects;

import baseobjects.BasePage;
import utils.customlocators.ByGlobalDataAttribute;
import utils.customlocators.ByTagText;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.Duration;
import java.util.List;

public class FlightSearchResults extends BasePage {

    private final By continueButtonLocator = new ByTagText("button", "Continue");
    private final By loadingBarLocator = By.className("loader-spacing");
    private final By nonstopCheckboxLocator = new ByGlobalDataAttribute(
            "test-id", "stops-0-label");
    private final By flightResultsLocator = new ByGlobalDataAttribute(
            "test-id","offer-listing");
    private final By addCarButtonLocator = new ByGlobalDataAttribute(
            "test-id", "xsellAddHotelNow");

    public FlightSearchResults(WebDriver driver){
        super(driver);
    }

    public String getTitle(){
        return driver.getTitle();
    }

    public void isLoaded(){
        wait.until(ExpectedConditions.urlContains("www.expedia.com/Flights-Search"));
        waitForSearchResults();
        wait.pollingEvery(Duration.ofMillis(100))
                .until(ExpectedConditions.elementToBeClickable(nonstopCheckboxLocator));
        flightList();
    }

    public void selectNonStop(){
        driver.findElement(nonstopCheckboxLocator).click();
        waitForSearchResults();
        wait.until(ExpectedConditions.elementToBeClickable(new ByTagText(
                "span", "Nonstop")));
        flightList();
    }

    public void checkPriceBelow(Double maxPrice){
        //Find lowest fare and check if assertion is true
        Number lFare = null;
        try {
            lFare = getLowestFare();
        } catch (
                ParseException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(lFare.doubleValue()<maxPrice, "No flights under $"+maxPrice);
    }

    public void selectFirstResultForFlight(){
        flightList().get(0).click();
        wait.until(ExpectedConditions.elementToBeClickable(continueButtonLocator));
        driver.findElement(continueButtonLocator).click();
    }

    public void addCar(){
        //Add car option
        driver.findElement(addCarButtonLocator).click();
    }

    private List<WebElement> flightList(){
        List<WebElement> allFlights = driver.findElements(flightResultsLocator);
        Assert.assertTrue(allFlights.size()>0, "No flights were found.");
        return allFlights;
    }

    public void waitForSearchResults() {
        //Waits for loading bar to appear and disappear
        wait.
                pollingEvery(Duration.ofMillis(10)).
                until(ExpectedConditions.visibilityOfElementLocated(loadingBarLocator));
        wait.
                pollingEvery(Duration.ofMillis(100)).
                until(ExpectedConditions.invisibilityOfElementLocated(loadingBarLocator));
    }

    private Number getLowestFare() throws ParseException {
        String sLowestFare = flightList().get(0).findElement(
                By.xpath("//*[@data-test-id='price-column']/div[1]/strong")).getText();
        NumberFormat format = NumberFormat.getCurrencyInstance();
        return format.parse(sLowestFare);
    }
}
