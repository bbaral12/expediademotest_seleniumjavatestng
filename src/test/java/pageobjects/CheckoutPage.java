package pageobjects;

import baseobjects.BasePage;
import utils.customlocators.ByTagText;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class CheckoutPage extends BasePage {

    public CheckoutPage(WebDriver driver){
        super(driver);
    }

    public void isLoaded(){
        wait.until(ExpectedConditions.urlContains("www.expedia.com/MultiItemCheckout"));
        wait.until(ExpectedConditions
                .visibilityOfElementLocated(new ByTagText("a","Debit/Credit Card")));
        Assert.assertEquals("Expedia: Payment", driver.getTitle());
    }
}
