package tests;

import baseobjects.BaseTest;
import dataproviders.BookFlights_DP;
import pageobjects.CarSearchResults;
import pageobjects.CheckoutPage;
import pageobjects.FlightSearchResults;
import pageobjects.HomePage;
import static utils.DateUtils.*;

import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.Map;

public class BookFlights extends BaseTest {


    @Test(dataProvider = "bookAFlightAndCar_DP", dataProviderClass = BookFlights_DP.class)
    public void bookAFlightAndCar(Map<Object, Object> map){

        HomePage home = new HomePage(getDriver());
        Assert.assertEquals(
                "Expedia Travel: Vacation Homes, Hotels, Car Rentals, Flights & More",
                home.getTitle());

        //Using tomorrow's date as flights are sometime not available after certain hours same day,
        //could be handled using other checks but wanted to keep it fairly simple for the exercise
        String departureDate = getTomorrowFormattedDate();
        String returnDate = getNextWeekFormattedDate();//1 week from today
        home.searchRoundTripFlights(
                map.get("Origin").toString(), map.get("Destination").toString(), departureDate, returnDate);

        FlightSearchResults flightResults = new FlightSearchResults(getDriver());
        flightResults.isLoaded();
        Assert.assertEquals(String.format("%s to %s flights",
                map.get("Origin").toString(), map.get("Destination").toString()), flightResults.getTitle());

        flightResults.selectNonStop();
        //flightResults.checkPriceBelow(Integer.parseInt(map.get("MaxPrice")));
        flightResults.checkPriceBelow(Double.valueOf((String)map.get("MaxPrice")));

        flightResults.selectFirstResultForFlight();
        flightResults.waitForSearchResults();
        flightResults.selectFirstResultForFlight();
        flightResults.addCar();

        //Switch to new tab
        for(String winHandle : getDriver().getWindowHandles()){
            getDriver().switchTo().window(winHandle);
        }

        CarSearchResults carResults = new CarSearchResults(getDriver());
        carResults.isLoaded();
        Assert.assertTrue(carResults.getTitle().contains("Car Rentals"));
        carResults.selectCarFilter(map.get("CarFilter").toString());
        carResults.selectCar();

        //Final Payment Page
        CheckoutPage checkoutPage = new CheckoutPage(getDriver());
        checkoutPage.isLoaded();
    }

}
