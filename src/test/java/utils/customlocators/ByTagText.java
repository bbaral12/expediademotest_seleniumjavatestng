package utils.customlocators;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import java.util.List;

public class ByTagText extends By {
    private final String tag;
    private final String text;

    public ByTagText(String tagName, String textToMatch) {
        this.tag = tagName;
        this.text = textToMatch;
    }

    @Override
    public List<WebElement> findElements(SearchContext context) {
        return context.findElements(By.xpath(
                String.format("//%s[text()='%s']", tag, text)
        ));
    }
}
