package utils;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ExcelUtils {
    private static XSSFSheet excelWSheet;
    private static XSSFWorkbook excelWBook;

    public static Object[][] getExcelDataTableArray(String filePath) throws IOException {

        FileInputStream excelFile = new FileInputStream(filePath);
        excelWBook = new XSSFWorkbook(excelFile);
        excelWSheet = excelWBook.getSheetAt(0);
        excelWBook.close();
        excelFile.close();

        int noOfRecords = excelWSheet.getLastRowNum()-1;//First Row has col titles
        int noOfFields = excelWSheet.getRow(0).getLastCellNum();

        Object[][] dataArray = new Object[noOfRecords+1][1];

        for (int i = 0; i < noOfRecords+1; i++) {
            Map<Object, Object> datamap = new HashMap<>();
            for (int j = 0; j < noOfFields; j++) {
                datamap.put(excelWSheet.getRow(0).getCell(j).toString(),
                        excelWSheet.getRow(i+1).getCell(j).toString());
            }
            dataArray[i][0] = datamap;
        }

        return dataArray;
    }
}
