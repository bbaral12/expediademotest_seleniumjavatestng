package utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtils {

    public static String getTomorrowFormattedDate() {
        LocalDateTime now = LocalDateTime.now().plusDays(1);
        return dateFormatter(now);
    }

    public static String getNextWeekFormattedDate() {
        LocalDateTime nextWeek = LocalDateTime.now().plusDays(8);
        return dateFormatter(nextWeek);
    }

    private static String dateFormatter(LocalDateTime date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM d");
        return  date.format(formatter);
    }
}
